CQ.Ext.ns("MyPortalSK");
MyPortalSK.Sidekick = {
    //add the button to sidekick bottom bar
    addMyPortalPreview: function(sk){
        var bbar = sk.getBottomToolbar();
        var dButton = bbar.getComponent(0);
        dButton = new CQ.Ext.Button({
            "icon": "/etc/designs/myportal/clientlibs/images/sidekickpreview.png",
            "tooltip": {
                "title": CQ.I18n.getMessage("MyPortal Preview"),
                "text": CQ.I18n.getMessage("Go to MyPortal Preview Page"),
                "autoHide": true
            },

            "handler": function(){
                var currentPagePath = CQ.WCM.getPagePath();
				var pvurl = CQ.HTTP.externalize("/bin/preview");
                pvurl = CQ.HTTP.addParameter(pvurl, "path", currentPagePath);
                var result = CQ.HTTP.get(pvurl);

				var wcpurl = "";
                var url = CQ.HTTP.externalize("/bin/config.mp.json");
                url = CQ.HTTP.addParameter(url, "param", "WCP_PREVIEW_HOST_URL");
                url = CQ.HTTP.addParameter(url, "_charset_", "utf8");
                var res = CQ.HTTP.get(url);
                if (CQ.HTTP.isOk(res)) {
                    wcpurl = res.responseText;
                } else {
                    wcpurl = "";
                }

                var aempath = CQ.HTTP.externalize("?article=" + window.location.protocol + "//" + window.location.host + currentPagePath + ".html?wcmmode=disabled");
				window.open(wcpurl + aempath);
            },
            "scope": this
        });
        bbar.insert(0, dButton );
    }
};
(function(){
    var E = MyPortalSK.Sidekick;
        CQ.WCM.on("sidekickready", function(sk){
                sk.on("loadcontent", function(){
 					var pagePath =  CQ.WCM.getPagePath();
                    if(pagePath.indexOf('/content/myportal/') == 0 ){
                        E.addMyPortalPreview(sk);
                    }
                });
        });
})();

