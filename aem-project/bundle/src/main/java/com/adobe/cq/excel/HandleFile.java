package com.adobe.cq.excel;
 
import java.io.IOException;
import java.io.InputStream;
import java.rmi.ServerException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.io.PrintWriter;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.commons.jcr.JcrConstants;

import org.apache.sling.api.resource.Resource;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
//Sling Imports
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//This is a component so it can provide or consume services
@SlingServlet(paths="/bin/upfile", methods = "POST", metatype=true)
public class HandleFile extends org.apache.sling.api.servlets.SlingAllMethodsServlet {

     private static final long serialVersionUID = 2598426539166789515L;
     
     /** Default log. */
     protected final Logger log = LoggerFactory.getLogger(this.getClass());

     private static final String PARENT_NODE = "/content/myportal/en-us/brandstandardsdata/allbrands";
     private static final String TEMPLATE_NAME = "/apps/myportal-ltd/templates/createbrandstandardstemplate"; 

     private static final String tagCategoryPrefix = "/etc/tags/myportal-ltd/brandstandards/categories/";
     private static final String tagSectionPrefix = "/etc/tags/myportal-ltd/brandstandards/sections/";
     private static final String tagCoveragePrefix = "/etc/tags/myportal-ltd/brandstandards/coverage/";
     private static final String tagRegionPrefix = "/etc/tags/myportal/region/";
     private static final String tagTierPrefix = "/etc/tags/myportal-ltd/brandstandards/tier/";
     private static final String tagBrandPrefix = "/etc/tags/myportal/brand/";

     //Inject a Sling ResourceResolverFactory
     @Reference
     private ResourceResolverFactory resolverFactory;
      
     // private Session session;

     @Reference
     private SlingRepository repository;

     public void bindRepository(SlingRepository repository) {
            this.repository = repository; 
     }


     @Override
     protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
         
    	 log.debug("Inside doPost().v3");

         try {
	         final boolean isMultipart = org.apache.commons.fileupload.servlet.ServletFileUpload.isMultipartContent(request);
	         PrintWriter out = null;

	         out = response.getWriter();
	         if (isMultipart) {
	             final java.util.Map<String, org.apache.sling.api.request.RequestParameter[]> params = request.getRequestParameterMap();
	             for (final java.util.Map.Entry<String, org.apache.sling.api.request.RequestParameter[]> pairs : params.entrySet()) {
	            	 
	            	 final String k = pairs.getKey();
	            	 final org.apache.sling.api.request.RequestParameter[] pArr = pairs.getValue();
	            	 final org.apache.sling.api.request.RequestParameter param = pArr[0];
	            	 final InputStream stream = param.getInputStream();

	            	 //Read & save the uploaded file into AEM
	            	 log.debug("Calling parseSpreadSheet...");
	            	 int excelValue = parseSpreadSheet(stream);
	            	 if (excelValue == 0)
	            		 out.println("Brand Standards data from the Excel Spread Sheet has been successfully imported into the AEM JCR");
	            	 else
	            		 out.println("Brand Standards data could not be imported into the AEM JCR");
	             }
	         }
         } catch (Exception e) {
        	 log.error("Exception in doPost()", e);
             e.printStackTrace();
         }
    	 log.debug("Exiting doPost().v3");
     }

     class PageSubComponent {

    	 public String componentJcrType;
    	 public String componentName;
    	 public HashMap<String, Object> attributes;

    	 public PageSubComponent(String type, String name, HashMap<String, Object> attr) {
    		 this.componentJcrType = type;
    		 this.componentName = name;
    		 this.attributes = attr;
    	 }
     }

 	 private int parseSpreadSheet(InputStream is) {
 		
 		log.debug("Inside parseSpreadSheet()");

        ResourceResolver resourceResolver = null;
        Session session = null;;
        
 		try {
            //Get the spreadsheet
        	Workbook workbook = Workbook.getWorkbook(is);
            Sheet sheet = workbook.getSheet(0);
            
            resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
            session = resourceResolver.adaptTo(Session.class);
            
            log.debug("Total Rows in Excel SpreadSheet: " + sheet.getRows());

            for (int row=1; row<sheet.getRows(); row++) {
            	// For Each Row

            	log.debug("Reading row # " + row);
                String nodeName = sheet.getCell(1, row).getContents();
                String pageTitle = sheet.getCell(2, row).getContents();
                Object tagCategories = getJcrAttributeData(sheet.getCell(3, row).getContents(), tagCategoryPrefix);
                Object tagSections = getJcrAttributeData(sheet.getCell(4, row).getContents(), tagSectionPrefix);
                Object tagCoverage = getJcrAttributeData(sheet.getCell(5, row).getContents(), tagCoveragePrefix);
                Object tagRegion = getJcrAttributeData(sheet.getCell(6, row).getContents(), tagRegionPrefix);
                Object tagTier = getJcrAttributeData(sheet.getCell(7, row).getContents(), tagTierPrefix);
                Object tagBrand = getJcrAttributeData(sheet.getCell(8, row).getContents(), tagBrandPrefix);
                String textComponentValue = sheet.getCell(9, row).getContents();

                log.debug("Row # " + row + ": title: " + pageTitle);

                HashMap<String, Object> props = new HashMap<String, Object>();
                if(null!=tagCategories) props.put("categories", tagCategories);
                if(null!=tagSections) props.put("sections", tagSections);
                if(null!=tagCoverage) props.put("coverage", tagCoverage);
                if(null!=tagRegion) props.put("region", tagRegion);
                if(null!=tagTier) props.put("tier", tagTier);
                if(null!=tagBrand) props.put("brand", tagBrand);
                
                HashMap<String, Object> componentAttributes = new HashMap<String, Object>();
                componentAttributes.put("sling:resourceType", "myportal/components/content/text");
                componentAttributes.put("text", textComponentValue);
                componentAttributes.put("textIsRich", "true");
                PageSubComponent textComponent1 = new PageSubComponent(JcrConstants.NT_UNSTRUCTURED, "text", componentAttributes);
                
                ArrayList<PageSubComponent> componentList = new ArrayList<PageSubComponent>();
                componentList.add(textComponent1);

                log.debug("Calling createPage for nodeName: " + nodeName + " with pageTitle: " + pageTitle);
                Page page = createPage(PARENT_NODE, nodeName, TEMPLATE_NAME, pageTitle, props, componentList, resourceResolver, session);
                log.debug("After createPage...");
            }

            log.debug("Saving and closing Session...");
            session.save(); 
            session.logout();

 			log.debug("Exiting parseSpreadSheet() with success");
 			return 0;
 		} catch(Exception ex) {
 			log.error("Exception in parseSpreadSheet()", ex);
 		} finally {
 			try {
 	             if (null!=session)
 	             	session.logout();
 			} catch(Exception ignoredex) { }
 		}

 		log.debug("Exiting parseSpreadSheet() with errors");
 		return -1;
 	 }

     private Page createPage(String location, String nodeName, String templateName, String pageTitle, 
    		 				HashMap<String, Object> customTags, ArrayList<PageSubComponent> componentList, 
    		 				ResourceResolver resourceResolver, Session session) {
    	 Page page = null;
    	 try {
    		 log.debug("Inside createPage(location: " + location + ", nodeName: " + nodeName + ", pageTitle: " + pageTitle + ")");

             PageManager pm =  resourceResolver.adaptTo(PageManager.class);
             log.debug("Calling AEM PageManager.create...");
             page = pm.create(location, nodeName, templateName, pageTitle); // e.g. "/content", "BLAHBLAHBLAH", "/apps/myportal-ltd/templates/createbrandstandardstemplate", "PAGE TITLE HERE");

             log.debug("Setting page attributes");
             if(null!=customTags) {
            	 Iterator<String> iterKeys = customTags.keySet().iterator();
            	 while(iterKeys.hasNext()) {
            		 String key = iterKeys.next();
            		 Object value = customTags.get(key);
            		 if(null!= key) {
            			 setPageAttribute(page, key, value);
            		 }
            	 }
             }
             log.debug("After setting page attributes");
             
             log.debug("Adding page sub-components");
             Iterator<PageSubComponent> componentsIter = componentList.iterator();
             while (componentsIter.hasNext()) {
            	 PageSubComponent comp = componentsIter.next();
            	 Node componentNode = addSubNode(page, comp.componentName, comp.componentJcrType);
            	 
                 log.debug("Setting component attributes");
                 if(null!=comp.attributes) {
                	 Iterator<String> attrKeys = comp.attributes.keySet().iterator();
                	 while(attrKeys.hasNext()) {
                		 String key = attrKeys.next();
                		 Object value = comp.attributes.get(key);
                		 if(null!= key) {
                			 setNodeAttribute(componentNode, key, value);
                		 }
                	 }
                 }
                 log.debug("After adding page sub-components");
             }
    	 } catch (Exception ex) {
    		 log.error("Exception in createPage()", ex);
    	 }
    	 return page;
     }

 	public void setPageAttribute(Page page, String name, Object value) {
        Node contentNode = page.getContentResource().adaptTo(Node.class);
        setNodeAttribute(contentNode, name, value);
 	}
	public Boolean setNodeAttribute(Node contentNode, String name, Object value) {
		try {
            if (value instanceof String) {
                contentNode.setProperty(name, (String) value);
            }
            else if (value instanceof String[]) {
                contentNode.setProperty(name, (String[]) value);
            }
            else if (value instanceof Long) {
                contentNode.setProperty(name, (Long) value);
            }
            contentNode.getSession().save();
            return true;
        }
        catch (RepositoryException e) {
        	log.error("Exception in setNodeAttribute(): Could not set node attribute", e);
        }
        return false;
	}

 	private Object getJcrAttributeData(String inString, String outPrefix) {
 		Object jcrData = null;
 		log.debug("Inside getJcrAttributeData(inString: " + inString + ", prefix: " + outPrefix + ")");
 		if(null!=inString && inString.length()>0) {
	        if(isMultiValued(inString))
	        	jcrData = getMultiValues((String)inString, outPrefix);
	        else {
	        	if(null!=outPrefix)
	        		jcrData = outPrefix + inString;
	        	else
	        		jcrData = inString;
	        }
 		}
 		log.debug("getJcrAttributeData(inString: " + inString + ", prefix: " + outPrefix + ") = " + jcrData);
 		return jcrData;
 	}
 	private boolean isMultiValued(String data) {
 		if(null!=data && (data.indexOf(",") > -1)) {
 			log.debug("isMultiValued(" + data + ") = true");
 			return true;
 		}
 		else {
 			log.debug("isMultiValued(" + data + ") = false");
 			return false;
 		}
 	}
 	private String[] getMultiValues(String inString, String outPrefix) {

 		log.debug("Inside getMultiValues()");
 		ArrayList<String> list = new ArrayList<String>();
 		try {
	 		StringTokenizer tokenizer = new StringTokenizer(inString, ",");
	 		if(null!=inString) {
		 		while(tokenizer.hasMoreTokens()) {
		 			String value = (String)tokenizer.nextToken();
		 			if(null!=value) {
		 				if(null!=outPrefix)
		 					value = outPrefix + value;
		 				list.add(value);
		 			}
		 		}
	 		}
	 		log.debug("Exiting getMultiValues()");
 		} catch(Exception ex) {
 			log.error("Exception in getMultiValues()", ex);
 		}

 		return list.toArray(new String[list.size()]);
 	}

 	private Node addSubNode(Page page, String nodeName, String nodeType) throws Exception {
 		Resource contentRes = page.getContentResource();
        Node contentNode = contentRes.adaptTo(Node.class);

        Node node = contentNode.addNode(nodeName, nodeType);

        node.getSession().save();
        return node;
 	}

    /* // Get data from the excel spreadsheet
     public int injectSpreadSheet(InputStream is) {
    	 log.debug("Inside injectSpreadSheet()");
         try {
            //Get the spreadsheet
        	Workbook workbook = Workbook.getWorkbook(is);
              
            Sheet sheet = workbook.getSheet(0);
              
            String firstName = ""; 
			String lastName = ""; 
			String address = "";
			String desc = "";

            for (int index=0; index<3;index++) {
                Cell a3 = sheet.getCell(0,index); 
                Cell b3 = sheet.getCell(1,index); 
                Cell c3 = sheet.getCell(2,index);
                Cell d3 = sheet.getCell(3,index);

                firstName = a3.getContents(); 
                lastName = b3.getContents(); 
                address = c3.getContents();
                desc = d3.getContents();
                log.debug("firstName: " + firstName + ", lastName=" + lastName + ", address=" + address + ", desc=" + desc);

                //Store the excel data into the Adobe AEM JCR
                injestCustData(firstName,lastName,address, desc);
             }    
            log.debug("Exiting injectSpreadSheet() with success");

            return 0;
         } catch(Exception e) {
            e.printStackTrace();
         }    

         log.debug("Exiting injectSpreadSheet() with error");
         return -1 ; 
     }

     //Stores customer data in the Adobe CQ JCR
     public int injestCustData(String firstName, String lastName, String address, String desc)
     {
         int num  = 0; 
         try { 
         
             //Invoke the adaptTo method to create a Session used to create a QueryManager
             ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
             session = resourceResolver.adaptTo(Session.class);

             //Create a node that represents the root node
             Node root = session.getRootNode(); 
                     
             //Get the content node in the JCR
             Node content = root.getNode("content");
                      
             //Determine if the content/customer node exists
             Node customerRoot = null;
             int custRec = doesCustExist(content);
         
                                            
             //-1 means that content/customer does not exist
             if (custRec == -1)
             {
                 //content/customer does not exist -- create it
                 customerRoot = content.addNode("customer");
             }
            else
            {
                //content/customer does exist -- retrieve it
                customerRoot = content.getNode("customer");
            }
                      
          int custId = custRec+1; //assign a new id to the customer node
                      
          //Store content from the client JSP in the JCR
         Node custNode = customerRoot.addNode("customer"+firstName+lastName+custId, "nt:unstructured"); 
               
             //make sure name of node is unique
         custNode.setProperty("id", custId); 
         custNode.setProperty("firstName", firstName); 
         custNode.setProperty("lastName", lastName); 
         custNode.setProperty("address", address);  
         custNode.setProperty("desc", desc);

         // Save the session changes and log out
         session.save(); 
         session.logout();
         return custId; 
         }
       
      catch(Exception  e){
          log.error("RepositoryException: " + e);
           }
      return 0 ; 
      } 
       
      */
      
     /*
      * Determines if the content/customer node exists 
      * This method returns these values:
      * -1 - if customer does not exist
      * 0 - if content/customer node exists; however, contains no children
      * number - the number of children that the content/customer node contains
     */
    /* private int doesCustExist(Node content) {
         try {
             int index = 0 ; 
             int childRecs = 0 ; 

         java.lang.Iterable<Node> custNode = JcrUtils.getChildNodes(content, "customer");
         Iterator it = custNode.iterator();

         //only going to be 1 content/customer node if it exists
         if (it.hasNext()) {
             //Count the number of child nodes in content/customer
             Node customerRoot = content.getNode("customer");
             Iterable itCust = JcrUtils.getChildNodes(customerRoot); 
             Iterator childNodeIt = itCust.iterator();
                   
             //Count the number of customer child nodes 
             while (childNodeIt.hasNext())
             {
                 childRecs++;
                 childNodeIt.next();
             }
              return childRecs; 
            } else
             return -1; //content/customer does not exist
         }
         catch(Exception e)
         {
             e.printStackTrace();
         }
         return 0;
      }
      */
      
}