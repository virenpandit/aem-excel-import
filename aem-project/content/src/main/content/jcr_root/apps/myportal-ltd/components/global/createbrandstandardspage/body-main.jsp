<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.text.SimpleDateFormat,java.util.Calendar,java.util.Date,java.util.GregorianCalendar"%>
<%@page import="com.wyn.myportal.util.*"%>

	<c:set var="pagetitle" value="<%=properties.get("jcr:title")%>" />
	<c:set var="publisheddate"
		value="<%=properties.get("displayDate",Date.class)%>" />
	<c:set var="brand" value="<%=properties.get("brand")%>" />
	<c:set var="region" value="<%=properties.get("region")%>" />
	<c:set var="categories" value="<%=properties.get("categories")%>" />
	<c:set var="coverage" value="<%=properties.get("coverage")%>" />
	<c:set var="sections" value="<%=properties.get("sections")%>" />


	<!-- Main Content Starts -->
	<div class="container-fluid">

		<div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<h1>Brand Standards Definition</h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 brand-matters">
				<c:if test="${(not empty pagetitle) && (pagetitle ne null)}">
					<h2>${pagetitle}</h2>
				</c:if>
				<div class="dates">
					<c:if test="${(not empty publisheddate) && (publisheddate ne null)}">
						<span><%=MyPortalUtil.findAPFormattedDate((Date)pageContext.getAttribute("publisheddate")) %></span>
					</c:if>
                </div>
                <div class="tags">
                    <c:choose>
						<c:when test="${(not empty brand) && (brand ne null)}">
                            <span><i class="ico ico-dim14x14 ico-tags">&nbsp;</i><b>BRANDS: </b>
                                <c:forEach var="brandItem" items="${brand}" varStatus="index">     
                                 <%=MyPortalUtil.getTagTitle((String)pageContext.getAttribute("brandItem"), resourceResolver,"") %>
                            <c:if test="${not index.last}">,</c:if>
                                </c:forEach> </span>
						</c:when>
                        <c:otherwise>
                            <span><i class="ico ico-dim14x14 ico-tags">&nbsp;</i>No Brands Selected</span>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="tags">
                    <c:choose>
                    	<c:when test="${(not empty region) && (region ne null)}">
                        	<span><i class="ico ico-dim14x14 ico-tags">&nbsp;</i><b>REGIONS: </b>
							<c:forEach var="regionItem" items="${region}" varStatus="index">     
	                         <%=MyPortalUtil.getTagTitle((String)pageContext.getAttribute("regionItem"), resourceResolver,"") %>
	                    	<c:if test="${not index.last}">,</c:if>
							</c:forEach> </span>
						</c:when>
                    	<c:otherwise>
                            <span><i class="ico ico-dim14x14 ico-tags">&nbsp;</i>No Regions Defined</span>
                        </c:otherwise>
                    </c:choose>
				</div>
                <c:choose>
                    <c:when test="${(not empty categories) && (categories ne null)}">
                        <div class="tags">
                            <i class="ico ico-dim14x14 ico-tags">&nbsp;</i><b>CATEGORIES: </b>
                            <c:forEach var="categoriesItem" items="${categories}" varStatus="index">
                                <%=MyPortalUtil.getTagTitle((String)pageContext.getAttribute("categoriesItem"), resourceResolver,"") %>
                                <c:if test="${not index.last}">,</c:if>
                            </c:forEach>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <span><i class="ico ico-dim14x14 ico-tags">&nbsp;</i>No Categories chosen</span>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${(not empty coverage) && (coverage ne null)}">
                        <div class="tags">
                            <i class="ico ico-dim14x14 ico-tags">&nbsp;</i><b>COVERAGE: </b>
                            <c:forEach var="coverageItem" items="${coverage}" varStatus="index">
                                <%=MyPortalUtil.getTagTitle((String)pageContext.getAttribute("coverageItem"), resourceResolver,"") %>
                                <c:if test="${not index.last}">,</c:if>
                            </c:forEach>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <span><i class="ico ico-dim14x14 ico-tags">&nbsp;</i>No Coverage defined</span>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${(not empty sections) && (sections ne null)}">
                        <div class="tags">
                            <i class="ico ico-dim14x14 ico-tags">&nbsp;</i><b>SECTIONS: </b>
                            <c:forEach var="sectionsItem" items="${sections}" varStatus="index">
                                <%=MyPortalUtil.getTagTitle((String)pageContext.getAttribute("sectionsItem"), resourceResolver,"") %>
                                <c:if test="${not index.last}">,</c:if>
                            </c:forEach>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <span><i class="ico ico-dim14x14 ico-tags">&nbsp;</i>No Sections defined</span>
                    </c:otherwise>
                </c:choose>

				<cq:include path="text" resourceType="/apps/myportal/components/content/text" />
                <cq:include path="par" resourceType="/apps/myportal/components/content/parsys" />

			</div>
		</div>

	</div>
	<!-- Main Content Ends -->

